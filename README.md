## Welcome to the ansible repository
The content is provided as model, adapt as needed. 

## Warning
The files and procedures detailed here where executed on Almalinux 8.

## Test case
For practice i have deployed 2 docker containers with Almalinux8, ssh service on port 22.

## Installation and usage
Check the file my-ansible-commands.txt from the repository.

## How to execute playbooks with inventories
If the inventory.yaml and playbook.yaml are in the same directory use:
ansible-playbook -i inventory.yaml playbook.yaml

<p>If you cloned this repository and each file is inside the proper directory use:
ansible-playbook -i inventories/inventory.yaml playbooks/playbook.yaml</p>

## Thank you and have fun :)
```
cd local_repo_folder
git clone https://gitlab.com/bogdanmarius/ansible.git
```
